/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 2/8/19.
//

#ifndef DS_ROV_SIM_RT_ROV_H
#define DS_ROV_SIM_RT_ROV_H

#include <Eigen/Dense>
#include "Thruster.h"

namespace ds_rov_sim_rt {

class Rov {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  Rov();

  typedef Eigen::Matrix<double, 6, Eigen::Dynamic> ThrusterMatrix;
  typedef Eigen::Matrix<double, 6, 1> ThrustVector;
  typedef std::pair<ThrustVector, ThrustVector> ThrustLimits;
  typedef Eigen::Matrix<double, 12, 12> StateMatrix;

  const Rov::ThrusterMatrix& getThrusterMatrix() const;
  const std::vector<Thruster>& getThrusters() const;

  const StateMatrix& getAddedMassDrag() const;
  const StateMatrix& getLinearDrag() const;
  const StateMatrix& getQuadraticDrag() const;

  const std::string& getBaseLinkname() const;
  const std::string& getCenterMassLinkname() const;
  const std::string& getCenterBuoyancyLinkname() const;
  const std::string& getCenterDragLinkname() const;

  const Eigen::Vector3d& vehicleBase() const;
  const Eigen::Vector3d& vehicleCenterBuoyancy() const;
  const Eigen::Vector3d& vehicleCenterDrag() const;

  ThrustLimits getMaxThrust() const;

  size_t numThrusters() const;

  double getNetBuoyancy() const;
  void setVariableBuoyancy(double net_vb);

  // TODO: Add functions to get force vectors

  void loadRosparam(ros::NodeHandle& nh, const std::string& parampath);

 protected:
  double net_variable_buoyancy_N;

  /// This is a 6-DOF thruster matrix, such that:
  /// thruster_matrix * thruster_cmds = thruster_force_and_torques_6DOF
  ThrusterMatrix thruster_matrix;

  /// This is a vector of thruster force <--> command models
  std::vector<Thruster> thruster_models;

  std::string baseLinkname;
  std::string centerMassLinkname;
  std::string centerBuoyancyLinkname;
  std::string centerDragLinkname;

  /// Location of the vehicle origin in the center-of-mass frame
  Eigen::Vector3d centerMass2base;

  /// Location of the center of buoyancy in the center-of-mass frame
  Eigen::Vector3d centerMass2centerBuoyancy;

  /// Location of the center of drag in the center-of-mass frame
  Eigen::Vector3d centerMass2centerDrag;

  double mass;

  /// Vehicle mass & moment of inertia, in kg (and kg * m^2, of course!)
  StateMatrix inertia;

  /// Base vehicle bouyancy in Newtons.  Positive is "up"
  double base_buoyancy;

  /// Variable vehicle buoyancy in Newtons (dropweights as well as VB).  Positive is "up"
  double variable_buoyancy;

  StateMatrix drag_added;
  StateMatrix drag_linear;
  StateMatrix drag_quadratic;

  void loadStateMatrix(ros::NodeHandle& nh, const std::string& parampath, StateMatrix& mat);
};

}

#endif //DS_ROV_SIM_RT_ROV_H
