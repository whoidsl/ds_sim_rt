/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 2/8/19.
//

#ifndef DS_ROV_SIM_RT_THRUSTER_H
#define DS_ROV_SIM_RT_THRUSTER_H

#include <Eigen/Dense>
#include <string>
#include <ros/node_handle.h>

namespace ds_rov_sim_rt {

// forward declaration
class ThrusterModel {
 public:
  ThrusterModel();

  double max_cmd;
  double quad_gain;
  double gain;
  double offset;
  double speed_gain;
  double speed_offset;
  double min_thrust;

  double calc_thrust(double cmd, double speed) const;
  double calc_cmd(double thrust, double speed) const;
  double max_thrust(double speed) const;

  static bool hasRosparamBlock(ros::NodeHandle& nh, const std::string& parampath);
  void loadRosparam(ros::NodeHandle& nh, const std::string& parampath);
  void updateRosparam(ros::NodeHandle& nh, const std::string& parampath);
};

class Thruster {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  Thruster();

  double calc_thrust(double cmd, double speed) const;
  double calc_cmd(double thrust, double speed) const;
  double max_fwd_thrust(double speed) const;
  double max_rev_thrust(double speed) const;

  const ThrusterModel& getForward() const;
  const ThrusterModel& getReverse() const;
  bool isPosPos() const;

  const std::string& getName() const;
  void setName(const std::string& _name);
  const std::string& getLinkName() const;
  void setLinkName(const std::string& _name);

  const std::string& getCmdTopicName() const;
  void setCmdTopicName(const std::string& _name);
  std::string getCmdTopicGlobalName(const std::string& actuators_ns) const;

  const std::string& getStateTopicName() const;
  void setStateTopicName(const std::string& _name);
  std::string getStateTopicGlobalName(const std::string& actuators_ns) const;

  const Eigen::Affine3d& getTransform() const;
  void setTransform(const Eigen::Affine3d& _tform);

  static bool hasRosparamBlock(ros::NodeHandle& nh, const std::string& parampath);
  void loadRosparam(ros::NodeHandle& nh, const std::string& parampath);
  void updateRosparam(ros::NodeHandle& nh, const std::string& parampath);

 protected:
  // Forward thrust model
  ThrusterModel forward;

  // Reverse thrust model
  ThrusterModel reverse;

  /// If true, positive command gives positive thrust
  bool pos_is_pos;

  /// This is a user-imposed max thrust limit
  double max_fwd_limit;
  double max_rev_limit;

  std::string name;
  std::string link_name;
  std::string cmd_topic_name;
  std::string state_topic_name;

  Eigen::Affine3d transform;
};

}

#endif //DS_ROV_SIM_RT_THRUSTER_H
