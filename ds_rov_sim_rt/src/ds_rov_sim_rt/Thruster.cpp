/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 2/8/19.
//

#include <cmath>
#include <ros/param.h>
#include "ds_rov_sim_rt/Thruster.h"

namespace ds_rov_sim_rt {

// ///////////////////////////////////////////////////////////////////////// //
// ThrusterModel
// ///////////////////////////////////////////////////////////////////////// //
ThrusterModel::ThrusterModel() {
  max_cmd = 0;
  quad_gain = 0;
  gain = 0;
  offset = 0;
  speed_gain = 0;
  speed_offset = 0;
  min_thrust = 1.0e-5;
}

/// Calculate thrust given a command and water speed.  Ignores signs.
/// \param cmd The command to the thruster, in "thruster command units"
/// \param speed The ambient speed along the +thrust axis, in m/s
/// \return The modelled thrust, in Newtons
double ThrusterModel::calc_thrust(double cmd, double speed) const {
  if (cmd > max_cmd) {
    cmd = max_cmd;
  }

  double value = quad_gain*cmd*cmd + gain*cmd + offset
      + speed_gain*cmd*speed + speed_offset*speed;
  if (value <= 0) {
    return 0;
  }
  return value;
}

/// Invert the thruster model and calculate the command required to achieve a given thrust
/// \param thrust The desired thrust, in Newtons
/// \param speed The ambient speed along the +thrust axis, in m/s
/// \return The command required to create that thrust, in "thruster command units"
double ThrusterModel::calc_cmd(double thrust, double speed) const {

  // Below a certain deadband threshhold, just shut off the thruster.
  if (thrust < min_thrust) {
    return 0;
  }

  // do the inversion.  It's trickier with the quadratic term

  // Remember the quadratic formula.  The solution to ax^2 + bx + c = 0
  // is given by [-b +/- sqrt(b^2 - 4 * a * c) ] / (2*a)
  // Here, we'll start by computing the discriminant (b^2 - 4*a*c) and ensuring its above zero
  double a = quad_gain;
  double b = gain + speed_gain*speed;
  double c = offset + speed_offset*speed - thrust;
  double descrim = b*b - 4*a*c;
  if (descrim < 0) {
    return 0;
  }

  // we only consider this root because for normal thruster functions, which
  // should have a positive "a" coefficient, everything will push this more positive
  double cmd = (-b + sqrt(descrim))/(2*a);

  // clamp to [0, max_cmd]
  if (cmd > max_cmd) {
    cmd = max_cmd;
  }
  if (cmd < 0) {
    cmd = 0;
  }

  return cmd;
}

/// Get the max thrust at a given speed.  Water flow against the thrust
/// direction is ignored (speed --> 0)
double ThrusterModel::max_thrust(double speed) const {
  return calc_thrust(max_cmd, speed);
}

/// Check if the given parameter path has ANY parameters to load.  Note that this
/// does not check for parameter VALIDITY-- that happens later.
/// \param nh The ROS node handle to use when looking up parameters
/// \param parampath The path to load parameters from
bool ThrusterModel::hasRosparamBlock(ros::NodeHandle& nh, const std::string& parampath) {
  return nh.hasParam(parampath + "/max_cmd") || nh.hasParam(parampath + "/quad_gain")
      || nh.hasParam(parampath + "/gain") || nh.hasParam(parampath + "/offset")
      || nh.hasParam(parampath + "/speed_gain") || nh.hasParam(parampath + "/speed_offset");
}

/// Load thruster parameters from the ROS parameter server, complaining if
/// anything goes wrong
/// \param nh The ROS node handle to use when looking up parameters
/// \param parampath The path to load parameters from
void ThrusterModel::loadRosparam(ros::NodeHandle& nh, const std::string& parampath) {
  if ( !nh.getParam(parampath + "/max_cmd",max_cmd)
      || !nh.getParam(parampath + "/gain",gain)
      || !nh.getParam(parampath + "/offset", offset) ) {
    ROS_FATAL("Thruster model REQUIRES max_cmd, gain & offset values in parameter namespace \"%s\"",
              parampath.c_str());
    ROS_BREAK();
  }

  if (nh.hasParam(parampath + "/speed_gain") != nh.hasParam(parampath + "/speed_offset")) {
    ROS_FATAL("Thrust model specified in \"%s\" should either have speed_gain AND speed_offset or neither",
              parampath.c_str());
    ROS_BREAK();
  }
  if (! nh.getParam(parampath + "/quad_gain", quad_gain) ) {
    quad_gain = 0.0;
  }
  if (! nh.getParam(parampath + "/speed_gain", speed_gain) ) {
    speed_gain = 0.0;
  }
  if (! nh.getParam(parampath + "/speed_offset", speed_offset) ) {
    speed_offset = 0.0;
  }
  if (! nh.getParam(parampath + "/min_thrust", min_thrust) ) {
    min_thrust = 1.0e-5; // in Newtons
  }
}

/// Update the thruster description from rosparam, not complaining
/// about any issues along the way.
/// \param nh The ROS node handle to use when looking up parameters
/// \param parampath The base parameter path to load from
void ThrusterModel::updateRosparam(ros::NodeHandle& nh, const std::string& parampath) {
  nh.getParam(parampath + "/max_cmd", max_cmd);
  nh.getParam(parampath + "/quad_gain", quad_gain);
  nh.getParam(parampath + "/gain", gain);
  nh.getParam(parampath + "/offset", offset);
  nh.getParam(parampath + "/speed_gain", speed_gain);
  nh.getParam(parampath + "/speed_offset", speed_offset);
  nh.getParam(parampath + "/min_thrust", min_thrust);
}

// ///////////////////////////////////////////////////////////////////////// //
// Thruster
// ///////////////////////////////////////////////////////////////////////// //
Thruster::Thruster() {
  pos_is_pos = true;
  max_fwd_limit = -1.0; // ignored
  max_rev_limit = -1.0; // ignored
}


/// Run the thruster model and calculate the thrust given a certain speed.
/// This will clamp speed to 0 when it doesn't match prop direction to avoid
/// introducing weird effects... in reality the thruster is significantly less
/// efficient in this regime, but we choose not to model that-- this is
/// uncommon for AUVs, and not important for ROVs operating near 0 speed.
/// \param cmd The command to the thruster, in "thruster command units"
/// \param speed The ambient speed along the +thrust axis, in m/s
/// \return The modelled thrust, in Newtons
double Thruster::calc_thrust(double cmd, double speed) const {
  // clamp speed when it opposes our thrust
  if ( (speed > 0 && cmd < 0) || (speed < 0 && cmd > 0) ) {
    speed = 0;
  }

  if (! pos_is_pos) {
    cmd *= -1.0;
  }

  if (cmd >= 0) {
    return forward.calc_thrust(fabs(cmd), fabs(speed));
  } else {
    return -reverse.calc_thrust(fabs(cmd), fabs(speed));
  }
}

/// Invert the thruster model and calculate the command required to achieve a given thrust
/// \param thrust The desired thrust, in Newtons
/// \param speed The ambient speed along the +thrust axis, in m/s
/// \return The command required to create that thrust, in "thruster command units"
double Thruster::calc_cmd(double thrust, double speed) const {
  // clamp speed when it opposes our thrust
  if ( (speed > 0 && thrust < 0) || (speed < 0 && thrust > 0) ) {
    speed = 0;
  }

  double ret;
  double abs_thrust = fabs(thrust);
  double abs_speed = fabs(speed);
  if (thrust >= 0) {
    if (abs_thrust > max_fwd_limit) {
      abs_thrust = max_fwd_limit;
    }
    ret =  forward.calc_cmd(abs_thrust, abs_speed);
  } else {
    if (abs_thrust > max_rev_limit) {
      abs_thrust = max_rev_limit;
    }
    ret =  -reverse.calc_cmd(abs_thrust, abs_speed);
  }

  if (! pos_is_pos) {
    ret *= -1.0;
  }

  return ret;
}

/// Get the max thrust at a given speed.  Water flow against the thrust
/// direction is ignored (speed --> 0)
double Thruster::max_fwd_thrust(double speed) const {
  if (max_fwd_limit > 0 ){
    return std::min(max_fwd_limit, forward.max_thrust(speed));
  }
  return forward.max_thrust(speed);
}

double Thruster::max_rev_thrust(double speed) const {
  if (max_rev_limit > 0 ){
    return std::min(max_rev_limit, reverse.max_thrust(speed));
  }
  return reverse.max_thrust(speed);
}


/// The forward thrust model
/// \return The forward thrust model
const ThrusterModel& Thruster::getForward() const {
  return forward;
}

/// Get the thruster model for reversing
/// \return The reverse thrust model
const ThrusterModel& Thruster::getReverse() const {
  return reverse;
}

/// Check if positive commands give positive thrust along the +X axis
/// \return True if positive commands give positive thrust
bool Thruster::isPosPos() const {
  return pos_is_pos;
}

/// Get the name of a thruster to be used in the config file
/// \return The name of this thruster
const std::string& Thruster::getName() const {
  return name;
}

/// Sets the name of the thruster.  Use sparingly.
/// \param _name The new name to set.
void Thruster::setName(const std::string& _name) {
 name = _name;
}

/// Get the name of a thruster's link
/// \return The name of the link with the +X axis pointing in the direction of +thrust
const std::string& Thruster::getLinkName() const {
  return link_name;
}

/// Get the name of a thruster's link
/// \param _link The name of the link with the +X axis pointing in the direction of +thrust
void Thruster::setLinkName(const std::string& _link) {
  link_name = _link;
}

/// Get the name of a thruster's command topic below the actuators namespace
/// \return The topic name as reported by the dynamics config space
const std::string& Thruster::getCmdTopicName() const {
  return cmd_topic_name;
}

/// Set the name of a thruster's command topic below the actuators namespace
/// \param _name The topic name to set
void Thruster::setCmdTopicName(const std::string& _name) {
  cmd_topic_name = _name;
}

/// Get the global name of the command topic
/// \param actuators_ns The actuators namespace
/// \return The global command topic name for this thruster
std::string Thruster::getCmdTopicGlobalName(const std::string& actuators_ns) const {
  if (cmd_topic_name[0] == '/') {
    return cmd_topic_name;
  }
  return actuators_ns + "/" + cmd_topic_name;
}

/// Get the name of a thruster's state topic below the actuators namespace
/// \return The topic name as reported by the dynamics config space
const std::string& Thruster::getStateTopicName() const {
  return state_topic_name;
}

/// Get the name of a thruster's state topic below the actuators namespace
/// \param _name The topic name to set
void Thruster::setStateTopicName(const std::string& _name) {
  state_topic_name = _name;
}

/// Get the global name of the state topic
/// \param actuators_ns The actuators namespace
/// \return The global state topic name for this thruster
std::string Thruster::getStateTopicGlobalName(const std::string& actuators_ns) const {
  if (state_topic_name[0] == '/') {
    return state_topic_name;
  }
  return actuators_ns + "/" + state_topic_name;
}

/// Get the transform from a center-of-mass centered body frame to the thruster
/// \return
const Eigen::Affine3d& Thruster::getTransform() const {
  return transform;
}

void Thruster::setTransform(const Eigen::Affine3d& _tform) {
  transform = _tform;
}

/// Check if the given parameter path has ANY parameters to load.  Note that this
/// does not check for parameter VALIDITY-- that happens later.
/// \param nh The ROS node handle to use when looking up parameters
/// \param parampath The path to load parameters from
bool Thruster::hasRosparamBlock(ros::NodeHandle& nh, const std::string& parampath) {
  return nh.hasParam(parampath + "/pos_is_pos") || nh.hasParam(parampath + "/max_thrust_limit")
    || ThrusterModel::hasRosparamBlock(nh, parampath + "/forward")
    || ThrusterModel::hasRosparamBlock(nh, parampath + "/reverse");
}

/// Load the thruster description from rosparam
/// \param nh The ROS node handle to use when looking up parameters
/// \param parampath The base parameter path to load from
void Thruster::loadRosparam(ros::NodeHandle& nh, const std::string& parampath) {

  forward.loadRosparam(nh, parampath + "/forward");

  if (reverse.hasRosparamBlock(nh, parampath + "/reverse")) {
    reverse.loadRosparam(nh, parampath + "/reverse");
  } else {
    ROS_WARN("No reverse thrust model detected in path \"%s\", using forward model...", parampath.c_str());
    reverse = forward;
  }

  nh.getParam(parampath + "/pos_is_pos", pos_is_pos);
  nh.getParam(parampath + "/max_fwd_limit", max_fwd_limit);
  nh.getParam(parampath + "/max_rev_limit", max_rev_limit);

  if (!nh.getParam(parampath + "/link", link_name) ) {
    link_name = name + "_link";
  }

  if (!nh.getParam(parampath + "/link", cmd_topic_name) ) {
    cmd_topic_name = name + "/cmd";
  }

  if (!nh.getParam(parampath + "/link", state_topic_name) ) {
    state_topic_name = name + "/state";
  }
}

/// Update the thruster description from rosparam, not complaining
/// about any issues along the way.
/// \param nh The ROS node handle to use when looking up parameters
/// \param parampath The base parameter path to load from
void Thruster::updateRosparam(ros::NodeHandle& nh, const std::string& parampath) {

  forward.updateRosparam(nh, parampath + "/forward");

  if (reverse.hasRosparamBlock(nh, parampath + "/reverse")) {
    reverse.updateRosparam(nh, parampath + "/reverse");
  } else {
    //ROS_WARN("No reverse thrust model detected in path \"%s\", using forward model...", parampath.c_str());
    reverse.updateRosparam(nh, parampath + "/forward");
  }

  nh.getParam(parampath + "/pos_is_pos", pos_is_pos);
  nh.getParam(parampath + "/max_fwd_limit", max_fwd_limit);
  nh.getParam(parampath + "/max_rev_limit", max_rev_limit);
  nh.getParam(parampath + "/link", link_name);
  nh.getParam(parampath + "/cmd_topic", cmd_topic_name);
  nh.getParam(parampath + "/state_topic", state_topic_name);
}

}

