/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 2/8/19.
//

#include "ds_rov_sim_rt/RovModelProcess.h"

namespace ds_rov_sim_rt {

RovModel::RovModel() : ds_base::DsProcess() {
}

RovModel::RovModel(int argc, char *argv[], const std::string &name)
: ds_base::DsProcess(argc, argv, name) {
}

void RovModel::onThrusterCmdMsg(const ds_actuator_msgs::ThrusterCmd::ConstPtr &msg, int index)
{
}

void RovModel::onAggregatedStateMsg(const ds_nav_msgs::AggregatedState::ConstPtr &msg)
{
}

void RovModel::onBuoyancyMsg(const ds_nav_msgs::Buoyancy::ConstPtr &msg)
{
}

void RovModel::setupParameters()
{
  ds_base::DsProcess::setupParameters();
}

void RovModel::setupPublishers()
{
  ds_base::DsProcess::setupPublishers();
}

void RovModel::setupSubscriptions()
{
  ds_base::DsProcess::setupSubscriptions();
}

void RovModel::setupTimers()
{
  ds_base::DsProcess::setupTimers();
}

}