/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 2/8/19.
//

#include <vector>
#include <string>

#include <tf2_eigen/tf2_eigen.h>
#include <tf2_ros/transform_listener.h>

#include "ds_rov_sim_rt/Rov.h"


namespace ds_rov_sim_rt {

const double GRAVITY = 9.81;

Rov::Rov() {
  mass = 1;
  inertia = StateMatrix::Identity();

  base_buoyancy = 0;
  variable_buoyancy = 0;

  drag_added = StateMatrix::Zero();
  drag_linear = StateMatrix::Zero();
  drag_quadratic = StateMatrix::Zero();
}

const Rov::ThrusterMatrix& Rov::getThrusterMatrix() const {
  return thruster_matrix;
}

const std::vector<Thruster>& Rov::getThrusters() const {
  return thruster_models;
}

const Rov::StateMatrix& Rov::getAddedMassDrag() const {
  return drag_added;
}

const Rov::StateMatrix& Rov::getLinearDrag() const {
  return drag_linear;
}

const Rov::StateMatrix& Rov::getQuadraticDrag() const {
  return drag_quadratic;
}

const std::string& Rov::getBaseLinkname() const {
  return baseLinkname;
}

const std::string& Rov::getCenterMassLinkname() const {
  return centerMassLinkname;
}

const std::string& Rov::getCenterBuoyancyLinkname() const {
  return centerBuoyancyLinkname;
}

const std::string& Rov::getCenterDragLinkname() const {
  return centerDragLinkname;
}

/// This model operates in body frame centered at the vehicle
/// center of mass.  However, sometimes it helps to get to the
/// vehicle origin or "base link".  Use this function to get the
/// location of the vehicle origin in the center-of-mass frame.
/// \return Vector from the center of mass to the vehicle origin,
/// in meters
const Eigen::Vector3d& Rov::vehicleBase() const {
  return centerMass2base;
}

/// \return Vehicle center of buoyancy relative to center of mass (in meters)
const Eigen::Vector3d& Rov::vehicleCenterBuoyancy() const {
  return centerMass2centerBuoyancy;
}

/// Return the maximum thrust and torque in the body frame
/// by summing the max thrust for each thruster
/// \return A pair with thrust limits in the positive and negative axes
Rov::ThrustLimits Rov::getMaxThrust() const {
  ThrustVector pos = ThrustVector::Zero(); // positive direction for each axis
  ThrustVector neg = ThrustVector::Zero(); // negative direction for each axis

  for (size_t i=0; i<thruster_models.size(); i++) {
    double max_fwd = thruster_models[i].max_fwd_thrust(0);
    double max_rev = thruster_models[i].max_rev_thrust(0);

    ThrustVector thruster_pos, thruster_neg;
    for (size_t ii=0; ii < 6; ii++) {
      if (thruster_matrix(ii, i) >= 0) {
        // if the matrix entry for this direction is >= 0, the
        // thruster is operating in the forward direction.  So use that limit.
        thruster_pos(ii) = fabs(max_fwd * thruster_matrix(ii,i));
        thruster_neg(ii) = fabs(max_rev * thruster_matrix(ii,i));
      } else {
        thruster_pos(ii) = fabs(max_rev * thruster_matrix(ii, i));
        thruster_neg(ii) = fabs(max_fwd * thruster_matrix(ii, i));
      }
    }
    pos += thruster_pos;
    neg += thruster_neg;
  }

  return std::make_pair(pos, neg);
}

/// \return Vehicle center of drag relative to center of mass (in meters)
const Eigen::Vector3d& Rov::vehicleCenterDrag() const {
  return centerMass2centerDrag;
}

/// Combine the base vehicle buoyancy, weight, and variable buoyancy and spit out the result.
/// \return Vehicle net buoyancy, in Newtons.  Positive is "floating"
double Rov::getNetBuoyancy() const {
  return base_buoyancy - mass*GRAVITY + variable_buoyancy;
}

/// Set the net variable buoyancy term, in Netwons
/// \param net_vb The next variable buoyancy value, in Newtons
void Rov::setVariableBuoyancy(double net_vb) {
  variable_buoyancy = net_vb;
}

void Rov::loadRosparam(ros::NodeHandle& nh, const std::string& parampath) {

  // Load our geometric information
  if (! (nh.getParam(parampath + "/base_link", baseLinkname)
      && nh.getParam(parampath + "/center_mass_link", centerMassLinkname)
      && nh.getParam(parampath + "/center_buoyancy_link", centerBuoyancyLinkname)
      && nh.getParam(parampath + "/center_drag_link", centerDragLinkname) ) ) {
    ROS_FATAL("ROV Dynamics model \"%s\" REQUIRES center_mass_link, center_buoyancy_link, and center_drag_link TF frame names",
        parampath.c_str());
    ROS_BREAK();
  }

  // Load our TF frame data
  tf2_ros::Buffer tfBuffer;
  tf2_ros::TransformListener tfListener(tfBuffer);

  geometry_msgs::TransformStamped com = tfBuffer.lookupTransform(baseLinkname, centerMassLinkname,
      ros::Time(0), ros::Duration(30));

  geometry_msgs::TransformStamped cob = tfBuffer.lookupTransform(baseLinkname, centerBuoyancyLinkname,
                                                                 ros::Time(0), ros::Duration(30));

  geometry_msgs::TransformStamped cod = tfBuffer.lookupTransform(baseLinkname, centerDragLinkname,
                                                                 ros::Time(0), ros::Duration(30));

  centerMass2base = tf2::transformToEigen(com).linear()*Eigen::Vector3d::Zero();
  centerMass2centerBuoyancy = tf2::transformToEigen(cob).linear()*Eigen::Vector3d::Zero();
  centerMass2centerDrag = tf2::transformToEigen(cod).linear()*Eigen::Vector3d::Zero();

  // Load the inertia matrix
  if (! nh.getParam(parampath + "/inertia/mass", mass)) {
    ROS_FATAL("ROV Dynamics model \"%s\" REQUIRES inertia/mass parameter (in kg)",
        parampath.c_str());
    ROS_BREAK();
  }
  inertia(0,0) = mass;
  inertia(1,1) = mass;
  inertia(2,2) = mass;

  std::vector<std::string> axes{"x", "y", "z"};
  for (size_t i=0; i<3; i++) {
    for (size_t j=0; j<3; j++) {
      bool loaded = nh.getParam(parampath + "/inertia/I" + axes[i] + axes[j],
          inertia(3+i, 3+j));
      if (i == j && !loaded) {
        ROS_FATAL("MUST specify moment of inertia for I%s%s", axes[i].c_str(), axes[j].c_str());
        ROS_BREAK();
      }
    }
  }

  // load the total system buoyancy states
  if (! nh.getParam(parampath + "/buoyancy_kg", base_buoyancy) ) {
    ROS_FATAL("MUST specify total system buoyancy!");
    ROS_BREAK();
  }
  if (! nh.getParam(parampath + "/buoyancy_kg_variable", variable_buoyancy)) {
    variable_buoyancy = 0.0;
  }

  // load the drag matrices
  loadStateMatrix(nh, parampath + "/drag_added", drag_added);
  loadStateMatrix(nh, parampath + "/drag_linear", drag_linear);
  loadStateMatrix(nh, parampath + "/drag_quadratic", drag_quadratic);

  // load the thruster models
  std::vector<std::string> thruster_names;
  if (!nh.getParam(parampath + "/thrusters", thruster_names)) {
    ROS_FATAL("Vehicle model \"%s\" defines NO thrusters!", parampath.c_str());
    ROS_BREAK();
  }

  // if we have one, load a default thruster
  Thruster defaultThruster;
  bool hasDefaultThruster = Thruster::hasRosparamBlock(nh, parampath + "/default_thruster");
  if (hasDefaultThruster) {
    ROS_INFO("Loading default thruster...");
    defaultThruster.loadRosparam(nh, parampath + "/default_thruster");
  } else {
    ROS_INFO("Loading default thruster...");
  }

  thruster_models.resize(thruster_names.size());
  thruster_matrix.resize(6, thruster_names.size());
  for (size_t i=0; i<thruster_models.size(); i++) {

    // First, load the thruster config
    std::string thruster_name = thruster_names[i];
    if (hasDefaultThruster) {
      thruster_models[i] = defaultThruster;
      thruster_models[i].setName(thruster_name);
      thruster_models[i].setLinkName(thruster_name + "_link");
      thruster_models[i].setCmdTopicName(thruster_name + "/cmd");
      thruster_models[i].setStateTopicName(thruster_name + "/state");
      thruster_models[i].updateRosparam(nh, parampath + "/" + thruster_name);
    } else {
      thruster_models[i].loadRosparam(nh, parampath + "/" + thruster_name);
    }

    ROS_INFO_STREAM("Loading position of thruster \"" <<thruster_models[i].getName()
                  <<"\" from link \"" <<thruster_models[i].getLinkName() <<"\"");
    geometry_msgs::TransformStamped tf_tform = tfBuffer.lookupTransform(
        centerMassLinkname, thruster_models[i].getLinkName(), ros::Time(0), ros::Duration(30));
    Eigen::Affine3d tform = tf2::transformToEigen(tf_tform);

    // keep a copy of the transform, just in case
    thruster_models[i].setTransform(tform);

    // build our thruster matrix entry
    Eigen::Vector3d force = tform.rotation() * Eigen::Vector3d::UnitX();
    thruster_matrix.block<3,1>(0,i) = force;
    thruster_matrix.block<3,1>(3,i) = tform.translation().cross(force); // moment
  }

}

/// Internal function to load a state matrix.  Any missing values are set to 0.
void Rov::loadStateMatrix(ros::NodeHandle& nh, const std::string& parampath, StateMatrix& mat) {
  std::vector<std::string> axes{"x", "y", "z", "r", "p", "h"};

  mat = StateMatrix::Zero();
  for (size_t i=0; i<6; i++) {
    for (size_t j=0; j<6; j++) {
      nh.getParam(parampath + "/" + axes[i] + axes[j], mat(i,j));
    }
  }
}

}